export function howUnlucky(year) {

    let unluckyCounter = 0
    
    for (let e of [...Array(12).keys()]) {
        let d = new Date(year,e,13)
        if (d.getDay() === 5) {
            unluckyCounter++
        }
    }
    return unluckyCounter
}

console.log(howUnlucky(2026))
