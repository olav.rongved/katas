import { howUnlucky } from './index'

describe('integration tests', function() {
    test('howUnlucky(2020) ➞2', function() {
        expect(howUnlucky(2020)).toBe(2)
    })

    test('howUnlucky(2026) ➞3', function() {
        expect(howUnlucky(2026)).toBe(3)
    })

    test('howUnlucky(2016) ➞1', function() {
        expect(howUnlucky(2016)).toBe(1)
    })
})