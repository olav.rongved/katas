import { getSplitIP,validateLengthIP,validateNoLeadingZerosIP,validateLastDigitNotZero,validateIPRange,isValidIP } from './index';

//unit?

describe('unit tests', function(){

    test('getSplitIP("1.2.3.4") ➞["1","2","3","4"]', function(){
        const ip = "1.2.3.4"
        console.log(JSON.stringify(getSplitIP(ip)));
        expect(JSON.stringify(getSplitIP(ip))).toEqual('["1","2","3","4"]')
    })

    test('validateLengthIP(["1","2","3","4"]) ➞ true', function(){
        const ip = ["1","2","3","4"]
        expect(validateLengthIP(ip)).toBe(true)
    })

    test('validateLengthIP(["1","2","3","4","5"]) ➞ false', function(){
        const ip = ["1","2","3","4","5"]
        expect(validateLengthIP(ip)).toBe(false)
    })

    test('validateNoLeadingZerosIP(["1","2","3","4"]) ➞ true', function(){
        const ip = ["1","2","3","4"]
        expect(validateNoLeadingZerosIP(ip)).toBe(true)
    })

    test('validateNoLeadingZerosIP(["1","02","3","4"]) ➞ false', function(){
        const ip = ["1","02","3","4"]
        expect(validateNoLeadingZerosIP(ip)).toBe(false)
    })

    test('validateLastDigitNotZero(["1.2.3.4"]) ➞ true', function(){
        const ip = "1.2.3.4"
        expect(validateLastDigitNotZero(ip)).toBe(true)
    })

    test('validateLastDigitNotZero(["1.2.3.0"]) ➞ false', function(){
        const ip = "1.2.3.0"
        expect(validateLastDigitNotZero(ip)).toBe(false)
    })

    test('validateIPRange(["1","2","3","4"]) ➞ true', function(){
        const ip = ["1","2","3","4"]
        expect(validateIPRange(ip)).toBe(true)
    })
    test('validateIPRange(["1","2","3","256"]) ➞ false', function(){
        const ip = ["1","2","3","256"]
        expect(validateIPRange(ip)).toBe(false)
    })
    
})

// Integration?
describe('Integration tests', function(){

    test('isValidIP("1.2.3.4") ➞true', function(){
        const ip = "1.2.3.4"
        expect(isValidIP(ip)).toBe(true)
    })

   
    test('isValidIP("1.2.3") ➞false', function(){
        const ip = "1.2.3"
        expect(isValidIP(ip)).toBe(false)
    })

    
    test('isValidIP("1.2.3.4.5") ➞false', function(){
        const ip = "1.2.3.4.5"
        expect(isValidIP(ip)).toBe(false)
    })

    test('isValidIP("123.45.67.89") ➞true', function(){
        const ip = "123.45.67.89"
        expect(isValidIP(ip)).toBe(true)
    })

    test('isValidIP("123.456.78.90") ➞false', function(){
        const ip = "123.456.78.90"
        expect(isValidIP(ip)).toBe(false)
    })

    test('isValidIP("123.045.067.089") ➞false', function(){
        const ip = "123.045.067.089"
        expect(isValidIP(ip)).toBe(false)
    })

})