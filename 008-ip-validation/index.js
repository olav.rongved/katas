export function getSplitIP(ip){
    return ip.split('.')
}

export function validateLengthIP(iparr,allowedLength = 4){
    return iparr.length === allowedLength
}

export function validateNoLeadingZerosIP(iparr){
    for (let e of iparr) {
        if (e[0] === '0'){
            return false
        }
    }
    return true
}

export function validateLastDigitNotZero(ip){
    return  getSplitIP(ip).reverse()[0] != '0'
}

export function validateIPRange(iparr) {
    const RANGE = [1,255]
    for (let e of iparr){
        let ipElementInt = parseInt(e)
        if (ipElementInt < RANGE[0] || ipElementInt > RANGE[1]) {
            return false
        }
    }
    return true
}

export function isValidIP(ip){
    const splitIP = getSplitIP(ip)

    return      validateLengthIP(splitIP) 
            &&  validateNoLeadingZerosIP(splitIP) 
            &&  validateIPRange(splitIP) 
            &&  validateLastDigitNotZero(ip)
}

//console.log(isValidIP("1.2.3.1"));
