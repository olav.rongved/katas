import { testJackpot } from './index.js'

describe('Slotmachine test for basic elements', function(){
// Numbers
test('["@", "@", "@", "@"] Should be true since all elements are equal', function(){
    expect( testJackpot(["@", "@", "@", "@"])).toBe(true) 
})
// string
test('["abc", "abc", "abc", "abc"] Should be true since all elements are equal', function(){
    expect( testJackpot(["abc", "abc", "abc", "abc"])).toBe(true) 
})
// string
test(' ["A", "a", "A", "a"] Should be false since all elements are not equal', function(){
    expect( testJackpot(["A", "a", "A", "a"])).toBe(false) 
})
test('["&&", "&", "&&&", "&&&&"] Should be false since all elements are not equal', function(){
    expect( testJackpot(["&&", "&", "&&&", "&&&&"])).toBe(false) 
})

})
describe('Slotmachine test for coercion', function(){
// big - test coercion
test('[0, false, 0, false] Should be false, if not, check for coercion!', function(){
    expect( testJackpot([0, false, 0, false])).toBe(false) 
})

test('[1, true, "1", true] Should be false, if not, check for coercion!', function(){
    expect( testJackpot([1, true, "1", true])).toBe(false) 
})
test('[arr, arrCopy, arr, arr] Should be false, if not, check for funky references!', function(){
    let arr = [1]
    let arrCopy = arr.map((x) => x)
    expect( testJackpot([arr, arrCopy, arr, arr])).toBe(false) 
})

test('[1],[1],[1],[1] Should be false, if not, check for funky references!', function(){
    expect( testJackpot([[1],[1],[1],[1]])).toBe(false) 
})
})

// Bonus tests: what about arrays? shallow? deep .. 
/* test('', function(){
    expect( testJackpot('cab')).toBe('abc') 
})
 */
