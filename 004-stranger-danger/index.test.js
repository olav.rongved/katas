import { noStrangers } from './index.js'

test('sentence should return correct stuff', function(){
    expect(JSON.stringify(noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly.")))
    .toBe('[["spot","see"],[]]')
})
