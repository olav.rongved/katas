export function noStrangers (sentence) {

    let words = sentence.replace(/\./g,'')
    let wordCount = {}

    let acquaintences = []
    let friends = []

    words = words.split(" ")
    words.map(word => {
        word = word.toLowerCase()

        if (!(word in wordCount)) {
            wordCount[word] = 1 
        } else {
        wordCount[word] += 1
        if (wordCount[word] >= 3 && !acquaintences.includes(word)) {

            acquaintences.push(word)
        } else if ( wordCount[word] >= 5 && !friends.includes(word) ) {
            friends.push(word)
        }
    }
    })

    return [acquaintences, friends]

};

//console.log(noStrangers("See Spot run. See Spot jump. Spot likes jumping. See Spot fly."))


//[["spot", "see"], []]