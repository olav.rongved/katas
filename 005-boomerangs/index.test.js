import { countBoomerangs } from './index'

describe('', function (){
    test('countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞2 due to 9,5,9 and 5,9,5', function(){
        expect(countBoomerangs([9, 5, 9, 5, 1, 1, 1])).toBe(2 ) 
    })

    test('countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞1 due to 6,7,6', function(){
        expect(countBoomerangs([5, 6, 6, 7, 6, 3, 9])).toBe(1) 
    })

    test('countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞0 due to no present boomerangs', function(){
        expect(countBoomerangs([4, 4, 4, 9, 9, 9, 9])).toBe(0) 
    })

    test('countBoomerangs([1, 7, 1, 7, 1, 7, 1]) ➞5 due to no present boomerangs', function(){
        expect(countBoomerangs([1, 7, 1, 7, 1, 7, 1])).toBe(5) 
    })

})
//countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞2
//countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞1
//countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞0
//countBoomerangs([1, 7, 1, 7, 1, 7, 1]) ➞5