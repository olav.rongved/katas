export function countBoomerangs(arr) {
    let counter = 0
    for (let i = 0; i < arr.length - 2; i++) {
        
        if (arr[i] === arr[i+2]){
            if (arr[i] === arr[i+1]){
                continue
            }
            counter++
        }
    }
    return counter
    
}
//countBoomerangs([9, 5, 9, 5, 1, 1, 1])
//countBoomerangs([5, 6, 6, 7, 6, 3, 9])
//countBoomerangs([4, 4, 4, 9, 9, 9, 9])
//countBoomerangs([1, 7, 1, 7, 1, 7, 1])

//countBoomerangs([9, 5, 9, 5, 1, 1, 1]) ➞2
//countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ➞1
//countBoomerangs([4, 4, 4, 9, 9, 9, 9]) ➞0
//countBoomerangs([1, 7, 1, 7, 1, 7, 1]) ➞5