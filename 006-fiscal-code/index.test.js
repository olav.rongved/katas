import { fiscalCode } from './index.js'

// Integration tests
test('Should return correctly', function(){
    const p = { name: "Matt",
                surname: "Edabit",
                gender: "M",
                dob: "1/1/1900"}
    expect(fiscalCode(p)).toBe("DBTMTT00A01")
})
    
test('Should return correctly', function(){
    const p = { name: "Helen",
                surname: "Yu",
                gender: "F",
                dob: "1/12/1950"}
    expect(fiscalCode(p)).toBe("YUXHLN50T41")
})

test('Should return correctly', function(){
    const p = { name: "Mickey",
                surname: "Mouse",
                gender: "M",
                dob: "16/1/1928"}
    expect(fiscalCode(p)).toBe("MSOMKY28A16")
})

// unit tests?
