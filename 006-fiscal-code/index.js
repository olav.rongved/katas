function checkConsonants (surname){
    const CONSONANTS = { 'A': 0, 'B': 1, 'C': 1, 'D': 1, 'E':0, 'F': 1, 'G': 1, 'H': 1, 'I':0,
                         'J': 1, 'K':1, 'L':1, 'M':1, 'N':1, 'O': 0, 'P':1,
                         'Q':1, 'R':1, 'S':1, 'T':1, 'U': 0, 'V':1, 'W':1,
                         'X':1, 'Z':1, 'Y':1}

    const surnameSplit = surname.toUpperCase().split('')

    const consonants = []
    const vowels = []
    for (let e of surnameSplit) {
        if (CONSONANTS[e]){
            consonants.push(e)
        } else {
            vowels.push(e)
        }
    }
    return [consonants, vowels]
}


function generateSurnameShort(surname) {

    const [consonants, vowels] = checkConsonants(surname)
    const shortName = []

    // if surname less than 3 length
    if (surname.length < 3){
        for (let e of consonants) {
            shortName.push(e)
        }
        for (let e of vowels) {
            shortName.push(e)
        }
        shortName.push('X')
    } else if (consonants.length >= 3) {
        for (let i = 0; i < 3; i++) {
            shortName.push(consonants[i])
        }
    } else {
        for (let e of consonants) {
            shortName.push(e)
        }
        if (shortName.length < 3) {
            for (let e of vowels) {
                if (shortName.length >= 3) {
                    break
                }
                shortName.push(e)
            }
        }
        
    }

    const initials = shortName.join('')
    return initials
    // if less than 3 consonants are present
}

function generateNameShort(name) {
    const [consonants, vowels] = checkConsonants(name)
    const shortName = []
    //console.log(consonants);
    //console.log(vowels);

    if (consonants.length === 3){
        for (let e of consonants) {
            shortName.push(e)
        }
    } else if(consonants.length > 3) {
        shortName.push(consonants[0])
        shortName.push(consonants[2])
        shortName.push(consonants[3])
    } else if (name.length < 3){
        for (let e of consonants){
            shortName.push(e)
        }
        for (let e of vowels){
            shortName.push(e)
        }
        
        shortName.push('X')
    }
    const initials = shortName.join('')
    return initials
}


function generateShortBirth(b) {
    const birthShort = []
    const s = b.split('')
    let c = 0
    for (let e of s.reverse()){
        
        if (c >= 2){
            break
        }
        birthShort.push(e)
        c++
    }
    return birthShort.reverse().join('')
}


function generateMonthConversion(b) {
    const months = { 1: "A", 2: "B", 3: "C", 4: "D",
                     5: "E", 6: "H",7: "L", 8: "M", 9: "P",
                      10: "R", 11: "S", 12: "T" }
    const date = b.split('/')
    return months[parseInt(date[1])]
}

function generateGenderbasedDob(b, gender){
    const date = b.split('/')
    let bday = date[0]

    if (gender === "M") {
        if (bday.length > 1){
            return bday
        } else {
            let temp = Array.from(bday)
            temp.unshift('0')
            return temp.join('')
        }
    } else {
        bday = (parseInt(bday) + 40).toString()
        return bday
    }
}


export function fiscalCode(person) {
    const fiscalCodeRes = []

    const surname = person['surname']
    const name  = person['name']
    const b  = person['dob']
    const gender  = person['gender']

    fiscalCodeRes.push(generateSurnameShort(surname))
    //console.log(fiscalCodeRes)
    fiscalCodeRes.push(generateNameShort(name))
    //console.log(fiscalCodeRes)
    fiscalCodeRes.push(generateShortBirth(b))
    //console.log(fiscalCodeRes)
    fiscalCodeRes.push(generateMonthConversion(b))
    //console.log(fiscalCode)
    fiscalCodeRes.push(generateGenderbasedDob(b, gender))
    //console.log(fiscalCode);

    return fiscalCodeRes.join('')
}

const t = fiscalCode({name: "Mickey",surname: "Mouse",gender: "M",dob: "16/1/1928"})
console.log(t);