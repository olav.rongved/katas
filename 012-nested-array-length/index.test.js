import { getLength } from './index'

describe('howdy', function () {
    test('getLength([1, [2, 3]]) ➞3', function () {
        expect(getLength([1, [2, 3]])).toBe(3)
    })

    test('getLength([1, [2, [3, 4]]]) ➞4', function () {
        expect(getLength([1, [2, [3, 4]]])).toBe(4)
    })

    test('getLength([1, [2, [3, [4, [5, 6]]]]]) ➞6', function () {
        expect(getLength([1, [2, [3, [4, [5, 6]]]]])).toBe(6)
    })

    test('getLength([1, [2], 1, [2], 1]) ➞5', function () {
        expect(getLength([1, [2], 1, [2], 1])).toBe(5)
    })
    test('getLength([]) ➞0', function () {
        expect(getLength([])).toBe(0)
    })
})
