function palSeq(n) {
    for (let seed = 1; seed <= n; seed++) {
        let current = seed;
        let step = 0;
        while (current < n) {
            const next = current + +[...`${current}`].reverse().join("");
            if (next === current * 2) break;
            current = next;
            step++;
        }
        if (current === n) return [seed, step];
    }
}

console.log(palSeq(99999999999));