import { getLength } from './index'

describe('howdy', function () {
    test('getLength([1, [2, 3]]) ➞3', function () {
        expect(getLength([1, [2, 3]])).toBe(3)
    })

})
