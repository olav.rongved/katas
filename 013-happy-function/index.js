function splitNumbers(n) {
    const strNumber = JSON.stringify(n)
    const listOfDigits = strNumber.split('')

    return listOfDigits
}


function sumDigitsSquared(stringDigits) {

    let sum = stringDigits.reduce(function (accumulator, currentValue) {
        return accumulator + parseInt(currentValue) ** 2
    }, 0)
    return sum
}

function transform(n) {
    const digits = splitNumbers(n)
    const res = sumDigitsSquared(digits)
    return res
}

function processNumber(number, steps, badNumbers) {
    if (number === 1) {
        console.log(`Happy number in ${steps}`);
        return
    }
    if ([4, 16, 37, 58, 89, 145, 42, 20].includes(number)) {
        if (badNumbers[number]) {
            console.log(`Sad number in ${steps}`);
            return
        }
        badNumbers[number] = true

    }

    steps++
    const newNumber = transform(number)
    processNumber(newNumber, steps,badNumbers)
    return number
}

function happyAlgorithm(number) {
    const badNumbers = {
        4: false,
        16: false,
        37: false,
        58: false,
        89: false,
        145: false,
        42: false,
        2: false
    }
    let steps = 0
    processNumber(number, steps, badNumbers)
}

happyAlgorithm(67)


