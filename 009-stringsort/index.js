
export function reorder (s) {

    const sarr = s.split('');

    let isNumber
    let isUpper
    let isLower

    const narr = []
    const uarr = []
    const larr = []

    for (let e of sarr){
        isNumber = Number.isInteger(parseInt(e))
        if (isNumber){
            narr.push(e)
        } else {
            isUpper = (e === e.toUpperCase())
            isLower = (e === e.toLowerCase())

            if (isUpper) {
                uarr.push(e)
            } else if (isLower) {
                larr.push(e)
            }
        }
        isNumber = false
        isUpper = false
        isLower = false
    }

    const reorderedArr = []

    for (let e of uarr){
        reorderedArr.push(e)
    }

    for (let e of larr){
        reorderedArr.push(e)
    }

    for (let e of narr) {
        reorderedArr.push(e)
    }

    

    

    const reorderedStr = reorderedArr.join('')
    return reorderedStr
}


console.log(
    reorder("hA2p4Py"));