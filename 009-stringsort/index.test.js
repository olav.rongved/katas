//imports

import { reorder } from './index'

//unit tests

// integration tests
describe('Integration tests : ', function() {

    test('reorder("hA2p4Py") ➞"Aphpy24"', function() {
        expect(reorder("hA2p4Py")).toBe("APhpy24")
    }),

    test('reorder("m11oveMENT") ➞"MENTmove11"', function() {
        expect(reorder("m11oveMENT")).toBe("MENTmove11")
    }),

    test('reorder("s9hOrt4CAKE") ➞"OCAKEshrt94"', function() {
        expect( reorder("s9hOrt4CAKE") ).toBe("OCAKEshrt94")
    })

})
