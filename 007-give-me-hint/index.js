export function grantTheHint(sentence) {
    let words = sentence.split(' ')
    
    let hiddenWords = getWords(words).reverse()

    return hiddenWords
    
}

function getWords(words){
    let hiddenWords = []
    let counterObject = {}
    words.forEach(function (item,idx) {
        
        counterObject[idx] = item.length
    })

    let keys = Object.keys(counterObject)

    // find current max indices
    let arr = Object.values(counterObject);
    let max = Math.max(...arr);

    while (max > 0){
    for (let e of keys){
        if (counterObject[e] === max) {
            words[e] = replaceLetter(words[e])
            counterObject[e]--
            arr = Object.values(counterObject);
            max = Math.max(...arr);
        }
    }

    hiddenWords.push([...words].join(' '))
    }
    return hiddenWords
    
}

function replaceLetter(word){
    let letters = word.split('')


    for (let i = letters.length-1; i >= 0; i--){
        if (letters[i] != '_'){
            letters[i] = '_'
            break
        }
    }
    return letters.join('')
}
console.log(grantTheHint("The Life of Pi"));

const res = `"____ _____ __ _____",
            "M___ Q____ o_ S____",
            "Ma__ Qu___ of Sc___",
            "Mar_ Que__ of Sco__",
            "Mary Quee_ of Scot_",
            "Mary Queen of Scots"]`
