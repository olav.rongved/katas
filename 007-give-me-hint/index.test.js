import { grantTheHint } from './index.js'

test(`"Mary Queen of Scots") ➞["____ _____ __ _____",
                                "M___ Q____ o_ S____",
                                "Ma__ Qu___ of Sc___",
                                "Mar_ Que__ of Sco__",
                                "Mary Quee_ of Scot_",
                                "Mary Queen of Scots"]`, 
function () {
    console.log(JSON.stringify(grantTheHint("Mary Queen of Scots")));

    const res = `["____ _____ __ _____","M___ Q____ o_ S____","Ma__ Qu___ of Sc___","Mar_ Que__ of Sco__","Mary Quee_ of Scot_"]`
    expect(JSON.stringify(grantTheHint("Mary Queen of Scots")))
            .toEqual(res)

})