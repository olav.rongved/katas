import { faroShuffle, shuffleCount } from './index.js';

let equals = (copy,copy2) => JSON.stringify(copy) === JSON.stringify(copy2)
describe('placeholder description for faroshuffle',function(){

    test('[1] ➞false - Card 1 should remain in the first position', function(){
        expect( faroShuffle([1])).toEqual(false) 
    })

    test('[1, 2, 3, 4, 5, 6, 7, 8] ➞[1, 5, 2, 6, 3, 7, 4, 8] - Card 1 should remain in the first position', function(){
        expect( equals(
            (faroShuffle([1, 2, 3, 4, 5, 6, 7, 8])) 
            ,[1, 5, 2, 6, 3, 7, 4, 8])
            ).toBe(true)
    })

    test('[1, 3, 5, 7, 2, 4, 6, 8] ➞[1, 2, 3, 4, 5, 6, 7, 8] should be back', function() {
        expect(equals(
            (faroShuffle([1, 2, 3, 4, 5, 6, 7, 8])) 
            ,[1, 5, 2, 6, 3, 7, 4, 8])
            ).toBe(true)
    })


})


describe('placeholder description for shuffleCount',function(){
    test('shuffleCount(8) ➞3', function(){
        expect( shuffleCount(8)).toBe(3) 
    })
    test('shuffleCount(14) ➞12', () => {
        expect( shuffleCount(14)).toBe(12) 
    })

    test('shuffleCount(52) ➞8', () => {
        expect( shuffleCount(52)).toBe(8) 
    })

    test('[1] ➞false - Card 1 should remain in the first position', function(){
        expect( faroShuffle(1)).toBe(false) 
    })

})
