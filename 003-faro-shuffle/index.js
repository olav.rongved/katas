export function faroShuffle(arr){

    if (arr.length < 2){
        return false
    }
    const half = Math.ceil(arr.length / 2)

    const arr1 = arr.splice(0,half)
    const arr2 = arr.splice(-half)
    
    return mergeArrays(arr1,arr2)
    
}

function mergeArrays(arr1,arr2){


    let newArr = []

    for (let i = 0; i < arr1.length; i++) {

        newArr.push(arr1[i])
        newArr.push(arr2[i])
    }
    return newArr
}


export function shuffleCount(n) {

    let c = 0

    if (n < 2){
        return false
    }

    let copy = [...Array(n).keys()]
    copy = copy.map(function(e){
        return e+1
    })

    let copy2 = [...Array(n).keys()]
    copy2= copy.map(function(e){
        return e
    })
    

    // test how many iterations by brute force
    let equals = (copy,copy2) => JSON.stringify(copy) === JSON.stringify(copy2)
    
    do {
        c++
        copy = faroShuffle(copy)
    }while(!equals(copy,copy2))
    return c
}
